---
title: The wild wayland
date: 2020-01-30 00:46:36
---


First off I want to give a huge thanks to the mir team and ubports community for all the amazing job they have done on mir and the unity8 stack! Without them we would not have been able to be where we are now!

## History
When we took over ubuntu touch and unity8 it’s stack ran only using the mirclient, and this has been fine for running just the halium devices, but we have always wanted to switch to wayland as thats the future of display protocols on linux. When we  took over we quickly started to look into alternatives and how we could move our stack over to a wayland based compositor as we thought the mir project would be suspended too, but we were really pleasantly surprised when canonical decided to continue the mir effort and transform it into a wayland compositor. For us this has been the best possible outcome as mir has grown into a really stable and solid compositor and I honestly think it has become one of the best (if not the) best compositor out there. The amount of work the incredible skilled mir developer have done is amazing! Mir has grown into the perfect convergence compositor we have always been dreaming of, it now does wayland, x11, window management, hybrid gpu, android backend and lots of other things.

## What’s mir, wayland and mirclient?
**Mir**: is a display server and compositor, simply said the mir's job is to take what the client gives us and “compose” it together and then display the content it on the screen.

**Mirclient**: is a client protocol that was built from the beginning of mir, but has since been deprecated and will be removed from mir in version 1.8. Think of the client protocol as the way the client (an application) talk to the server (mir) to be able to display it’s content on the screen.

**Wayland**: is a client protocol like mirclient, but built to be standalone from the server itself and can be implemented in any server (like mir, mutter, kwin, etc). 

So we are replacing the protocol not the compositor, we will still use mir as mir now support the wayland protocol.

## Why switch to wayland?
So there are many reasons why we wanted to move away from mirclient and over to wayland, the first obvious one is, everyone else uses wayland this means we can run all wayland based apps on unity8 without any problems. The second reason is that mirclient is already is deprecated and will be removed from mir in version 1.8. Third mirclient requires patches to mesa to be able to run, this is not a good solution, this also boils into the last point, be able to run unity8 on any distro, as porting the mesa patches over to other distros will never happen.

## The development on our side

Our road from mirclient to wayland has been bumpy, and we are still not at the finish line. The reason for this has been how the whole stack was designed around the mirclient. There have been many things we have needed to rework and we still have a number of things that still needs to be reworked. Some examples of this is trust store uses the mirclient to display the prompt in a secure way to decide if you want to give permission to use a component (like camera) to an application. One of the other big things has been to rework how to run a nested compositor (where we have one compositor running as root and one on top of the root one as user) using wayland instead of the mirclient

With all this work done, we now are able to run our stack on upstream mesa stack! This is what we use for the pinephone and is the reason why porting to that device has been a challenging part. What this also brings is the ability to run on other distros! We have seen this with Arch, postmarkedOS, Fedora and Debian getting the unity8 stack ported! And I hope with all this we will see unity8 running on many distros and flavours this year :) #2020unity8 anyone?

All this work has been done in parallel with the work done on our newest unity8 with upstream mir that will be released on ota-12. So to say it has been busy and a lot of work is an understatement :) The work has been intense, but we love it! Where we are today is amazing with the small team we got! 
