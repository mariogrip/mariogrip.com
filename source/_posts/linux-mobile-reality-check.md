---
title: Linux mobile, daily driver reality check. 
date: 2020-01-08 00:22:36
---

In this post I will do some reality check on Linux mobile as daily driver alternatives, I’m not taking about experimenting, testing or just for fun scenarios. This post is aimed at the average Joe, if you’re a Desktop Linux Ninja you probably know your way around the issues I will list.

## “Standard” desktop apps

This one I see a lot in Linux communities, the expectation of running “standard” Linux desktop apps (like gimp) on mobile Linux devices. Sure it technically possible to do so, but it wont be usable nor pleasant to use. These apps are not in any way designed to run on a small 5inch touch screen. I agree it’s cool to run gimp on your phone, but it’s not useful. These apps might adapt to a mobile interface in the future, but right now this is not something you could use, other than “this looks cool” :)

## Middleware, Desktop environments, services, etc

The list of desktop environments for Linux mobile are growing, this is amazing! But one thing I have seen is that people expect these DE’s to be daily driver ready from day 1, and this is simply not the case. Creating a DE for mobile phones are super hard and it will take long time to develop a DE that is usable for day to day use on a phone. A phone DE has a lot more moving parts then what a desktop DE has, so it will take a lot of effort to implement all the features that exist on a phone. It might seem like a desktop DE should just work, but this is far from the truth. A good example is the different app management and app lifecyle, on a phone you dont want the gui of an app running in the background when not used, as that would be way to heavy on the battery, therefor you need to design the DE to suspend the apps gui when not in focus. This is one of many examples, if you start looking at all the parts that go into your current phone you will quickly see what I mean. And those are only the part you see, there is plenty going on in the backend. 

There is also a lot of different components that require special services on a phone that does not exist on a desktop, like call, sms, gps, background notifications, leds, quick wakeup sleep mode and a lot more. These services will also take time to develop. An example for this is oFono thats used by ubports and kde to allow cellular to work (sms, calls, data). But this is an area where different OS and DE makers could work together on developing and fixing bugs to create much better and stable services, but some seem to have more of a not-invented-here syndrome then other.

It also comes back to stability, something I will touch on in next paragraph, but the same apply to the DE/services, it needs to be stable.

### Be honest!

One thing the gnu/linux mobile, there is a lot of bogus claims. This is really infuriating for us that spend hours of free-time building a OS that already is daily driver ready... The biggest issue I have with these claims is that it tricks people into thinking the claims is the superior and *better* option when that might not be the case, and this might then result in belief that that’s how all other options are too.

These bogus claims damages the overall view and reputation of Linux mobile.

The truth is that Ubuntu touch (and arguably luneOS) is the alredy daily driver ready fully open source gnu/Linux OS, we dont have any real numbers on how many devices run ubuntu touch (as we dont track anyone period) but based on the download numbers and people in our telegram channel, we defently have well over 5000 active daily users! Some are close, like Plasma but its not there yet. This has little to do with UBports, this has to do with all the amazing work (and millions invested) canonical has done before we took it over! You don't have to wait, Linux mobile is already here and has been here for years. Only difference today is all the mainline hardware.

Although ubuntu touch is daily driver ready, that does not mean it has all the apps required for all people to switch yet, many people use them along side another phone, as ubuntu touch does not support many of the android apps that may be required, like in my case I need to use a android app to pay my bills, so i got a wifi only device just to pay my bills, thats really annoying. But I also have snapchat and vipps (norwegian banking app) because those simply does not have an alternatives on ubuntu touch.

*NOTE* sailfish is not fully open source (it has closed source GUI, DE and applications), but it's considerd daily driver ready. I have used it before and its pretty nice, but sadly it's not fully open source.

## Stability, app installation and updates

One thing I also see a lot is the expectation to reliable use package managers and read-write rootfs on mobile phones, and yes this is fine if you know what you’re doing. But for the average Joe having to deal with these kinds of issues is not ideal. The reason it’s not ideal to use for example apt to update every part of the system (except for dev work and testing) is that phones need to be a lot more stable and reliable. 

A phone is different from a desktop in the way we heavily rely on phones, like for example to be able to make a call in an emergency situation. And on top of this many people only have mobile phone to be able to communicate, as normal in-house phones are getting more and more removed from homes. The difference is if a desktop have a bad install/update and/or fails to boot, it’s not that big of a deal as someone can drop into recovery and fix to try to fix the issue. On phones, you will need another device to fix these issues, and this is not always possible to do if you’re not home.

Using package managers like apt for developing and hacking works just fine. It's not ideal to use apt to update/install every part of the system because need to be a lot more stable and reliable. You don’t want your phone to be stuck with conflicting packages while you’re out and about. This is the same for installing apps with a package manager on the root filesystem, this can (and probably will sometime) result in unwanted results as for example unwanted packages can mess with the root system. 

Think about this scenario, you’re in a 20h train ride, you install an app with apt it installs a random service that messes with your compositor and baam you cant use your phone unless you remove it, and since your compositor is not working anymore you cant do this without needing to connect your phone to a desktop, but problem is you didn't bring a desktop with you. This is situations that may happen, and these are completely avoided by having a read only filesystem where the root system will always be untouched. 

And with image based updates, the images will be checksum-ed and GPG signed, so you will always get a safe update from known gpg signers. And since it’s image based, it will always contain the same rootfs across all devices. 

Simply, You don’t want your phone to be stuck with conflicting packages while you’re out and about. There is a really good reason why Canonical choose to go with click, system-image and read-only root filesystem by default on UbuntuTouch and for the same reason UBports will continue to do so. With that being said, you can use apt and read-write filesystem if you want, this is only whats default, but you will have full sudo access to do whatever you want. 
